package com.weatherbit.datamodel;

import org.joda.time.DateTime;

public class Data {
	private int rh;

	private String pod;

	private double lon;

	private double pres;

	private String timezone;

	private String ob_time;

	private String country_code;

	private int clouds;

	private int ts;

	private int solar_rad;

	private String state_code;

	private String city_name;

	private double wind_spd;

	private String wind_cdir_full;

	private String wind_cdir;

	private double slp;

	private int vis;

	private String h_angle;

	private String sunset;

	private int dni;

	private double dewpt;

	private int snow;

	private int uv;

	private int precip;

	private int wind_dir;

	private String sunrise;

	private int ghi;

	private int dhi;

	private String aqi;

	private double lat;

	private Weather weather;

	private String datetime;

	private double temp;

	private String station;

	private double elev_angle;

	private double app_temp;

	public void setRh(int rh) {
		this.rh = rh;
	}

	public int getRh() {
		return this.rh;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getPod() {
		return this.pod;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getLon() {
		return this.lon;
	}

	public void setPres(double pres) {
		this.pres = pres;
	}

	public double getPres() {
		return this.pres;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getTimezone() {
		return this.timezone;
	}

	public void setOb_time(String ob_time) {
		this.ob_time = ob_time;
	}

	public String getOb_time() {
		return this.ob_time;
	}

	public void setCountry_code(String country_code) {
		this.country_code = country_code;
	}

	public String getCountry_code() {
		return this.country_code;
	}

	public void setClouds(int clouds) {
		this.clouds = clouds;
	}

	public int getClouds() {
		return this.clouds;
	}

	public void setTs(int ts) {
		this.ts = ts;
	}

	public int getTs() {
		return this.ts;
	}

	public void setSolar_rad(int solar_rad) {
		this.solar_rad = solar_rad;
	}

	public int getSolar_rad() {
		return this.solar_rad;
	}

	public void setState_code(String state_code) {
		this.state_code = state_code;
	}

	public String getState_code() {
		return this.state_code;
	}

	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}

	public String getCity_name() {
		return this.city_name;
	}

	public void setWind_spd(double wind_spd) {
		this.wind_spd = wind_spd;
	}

	public double getWind_spd() {
		return this.wind_spd;
	}

	public void setWind_cdir_full(String wind_cdir_full) {
		this.wind_cdir_full = wind_cdir_full;
	}

	public String getWind_cdir_full() {
		return this.wind_cdir_full;
	}

	public void setWind_cdir(String wind_cdir) {
		this.wind_cdir = wind_cdir;
	}

	public String getWind_cdir() {
		return this.wind_cdir;
	}

	public void setSlp(double slp) {
		this.slp = slp;
	}

	public double getSlp() {
		return this.slp;
	}

	public void setVis(int vis) {
		this.vis = vis;
	}

	public int getVis() {
		return this.vis;
	}

	public void setH_angle(String h_angle) {
		this.h_angle = h_angle;
	}

	public String getH_angle() {
		return this.h_angle;
	}

	public void setSunset(String sunset) {
		this.sunset = sunset;
	}

	public String getSunset() {
		return this.sunset;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getDni() {
		return this.dni;
	}

	public void setDewpt(double dewpt) {
		this.dewpt = dewpt;
	}

	public double getDewpt() {
		return this.dewpt;
	}

	public void setSnow(int snow) {
		this.snow = snow;
	}

	public int getSnow() {
		return this.snow;
	}

	public void setUv(int uv) {
		this.uv = uv;
	}

	public int getUv() {
		return this.uv;
	}

	public void setPrecip(int precip) {
		this.precip = precip;
	}

	public int getPrecip() {
		return this.precip;
	}

	public void setWind_dir(int wind_dir) {
		this.wind_dir = wind_dir;
	}

	public int getWind_dir() {
		return this.wind_dir;
	}

	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}

	public String getSunrise() {
		return this.sunrise;
	}

	public void setGhi(int ghi) {
		this.ghi = ghi;
	}

	public int getGhi() {
		return this.ghi;
	}

	public void setDhi(int dhi) {
		this.dhi = dhi;
	}

	public int getDhi() {
		return this.dhi;
	}

	public void setAqi(String aqi) {
		this.aqi = aqi;
	}

	public String getAqi() {
		return this.aqi;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLat() {
		return this.lat;
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	public Weather getWeather() {
		return this.weather;
	}

	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}

	public String getDatetime() {
		return this.datetime;
	}

	public void setTemp(double temp) {
		this.temp = temp;
	}

	public double getTemp() {
		return this.temp;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getStation() {
		return this.station;
	}

	public void setElev_angle(double elev_angle) {
		this.elev_angle = elev_angle;
	}

	public double getElev_angle() {
		return this.elev_angle;
	}

	public void setApp_temp(double app_temp) {
		this.app_temp = app_temp;
	}

	public double getApp_temp() {
		return this.app_temp;
	}
}

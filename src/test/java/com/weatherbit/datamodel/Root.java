package com.weatherbit.datamodel;

import java.util.List;
public class Root
{
    private List<Data> data;

    private int count;

    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public void setCount(int count){
        this.count = count;
    }
    public int getCount(){
        return this.count;
    }
}

package com.weatherbit.stepdef;

import java.util.List;

import org.junit.Assert;

import com.weatherbit.datamodel.Data;
import com.weatherbit.datamodel.Root;
import com.weatherbit.datamodel.Weather;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import io.restassured.RestAssured;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;

public class GetCurrentWeatherDataStepDef {
    RequestSpecification request;
    Response             response;
    Root                 responseBody;



    @And("User authenticate the API request with {string}")
    public void user_authenticate_the_API_request_with(String accesskey) {
        request = given().queryParam("key", accesskey);
    }

    @When("User sends HTTP request and get the response {string}")
    public void user_sends_HTTP_request_and_get_the_responce(String path) {
        response = request.get(path);
    }

    @Given("User sets current weather api endpoint {string}")
    public void user_sets_current_weather_api_endpoint(String baseUrl) {
        RestAssured.baseURI = baseUrl;
    }

    @Given("User sets the parameter {double} and {double}")
    public void user_sets_the_parameter_and(Double lon, Double lat) {
        request.queryParam("lon", lon).queryParam("lat", lat);
    }

    @When("User sets the parameter postcode {int}")
    public void user_sets_the_parameter_postcode(int postcode) {
        request.queryParam("postal_code", postcode);
    }

    @Then("verify status code {int}")
    public void verify_status_code(int status_code) {
        Assert.assertEquals(status_code, response.getStatusCode());
    }

	@Then("get the value of the timestamp_utc, weather for all data entries")
	public void get_the_value_of_the_timestamp_utc_weather_for_all_data_entries() {
		responseBody = response.as(Root.class);

		List<Data> data = responseBody.getData();

		if (data.size() == 1) {
			Weather weatherDetails = data.get(0).getWeather();

			System.out.println("code:" + weatherDetails.getCode());
			System.out.println("description:" + weatherDetails.getDescription());
			System.out.println("icon" + weatherDetails.getIcon());
			System.out.println("code:" + data.get(0).getOb_time());
		}
	}

	@And("get the value present in the field {string} and verify {string}")
	public void get_the_value_present_in_the_field_and_verify(String field, String expvalue) {
		String statecodeValue = null;

		responseBody = response.as(Root.class);

		List<Data> data = responseBody.getData();

		if (data.size() == 1) {
			statecodeValue = data.get(0).getState_code();
		}

		Assert.assertEquals(expvalue, statecodeValue);
	}

}


package com.weatherbit.testrunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "src\\main\\resources\\Features", glue = {
		"com.weatherbit.stepdef" },monochrome=true,plugin = { "pretty", "html:target\\HtmlReports" })
public class TestRunner {

}

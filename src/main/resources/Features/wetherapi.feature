Feature: validating weather api

  Background: set the base url and accesskey
    Given User sets current weather api endpoint "https://api.weatherbit.io"
    And User authenticate the API request with "133787457cd8448c84a78c61686e5121"

    @regression
  Scenario Outline: Get the statecode based on lattitude and longitude
    Given User sets the parameter <lon> and <lat>
    When User sends HTTP request and get the response "<path>"
    Then verify status code <status_code>
    And get the value present in the field "<field>" and verify "<expvalue>"

    Examples: 
      | path         | lon       | lat        | field      | expvalue | status_code |
      | v2.0/current | 40.730610 | -73.935242 | state_code | NY       |         200 |

  #Examples:
  #| baseUrl                    | accesskey                        | path         | lon       | lat        | field      | expvalue | status_code |
  #| https://api.weatherbit.io/ | 133787457cd8448c84a78c61686e5121 | v2.0/current | 40.730610 | -73.935242 | state_code | NY       |         200 |
  #
  @smoke
  Scenario Outline: Get 3 hourly weather forecast from postcode
    Given User sets the parameter postcode <postcode>
    When User sends HTTP request and get the response "<path>"
    Then verify status code 200
    And get the value of the timestamp_utc, weather for all data entries

    Examples: 
      | path         | postcode |
      | v2.0/current |     2145 |
